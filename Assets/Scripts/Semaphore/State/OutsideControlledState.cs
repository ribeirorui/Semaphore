namespace Semaphore.State
{
	/// <summary>
	/// State that never change itself, only by outside controller
	/// </summary>
	public class OutsideControlledState : SemaphoreState
	{
		public override bool IsFinished { get; protected set; }

		public override void Activate()
		{
			IsFinished = false;
			signal.Activate();
		}

		public override void Deactivate()
		{
			IsFinished = true;
			signal.Deactivate();
		}
	}
}