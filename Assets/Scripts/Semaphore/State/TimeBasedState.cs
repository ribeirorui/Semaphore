using UnityEngine;

namespace Semaphore.State
{
	/// <summary>
	/// State with automatic deactivation by time
	/// </summary>
	public class TimeBasedState : SemaphoreState
	{
		[SerializeField] private float activeTime;

		private float _deactivationTime;

		public override bool IsFinished { get; protected set; }

		public override void Activate()
		{
			enabled = true;
			_deactivationTime = Time.time + activeTime;
			IsFinished = false;
			signal.Activate();
		}

		public override void Deactivate()
		{
			IsFinished = true;
			enabled = false;
			signal.Deactivate();
		}

		private void Update()
		{
			if (Time.time < _deactivationTime) return;

			Deactivate();
		}
	}
}