using Semaphore.Signals;
using UnityEngine;

namespace Semaphore.State
{
	public abstract class SemaphoreState : MonoBehaviour
	{
		[SerializeField] protected Signal signal;

		public abstract bool IsFinished { get; protected set; }
		public abstract void Activate();
		public abstract void Deactivate();
	}
}