﻿using System;
using System.Collections.Generic;
using Semaphore.State;
using Semaphore.Transitions;
using UnityEngine;

namespace Semaphore
{
	public class SemaphoreController : MonoBehaviour
	{
		[SerializeField] private List<Transition> transitions;
		[SerializeField] private SemaphoreState initialState;
		[SerializeField] private SemaphoreState disabledState;

		private SemaphoreState _currentState;

		private readonly HashSet<SemaphoreState> _availableStates = new HashSet<SemaphoreState>();

		public SemaphoreState CurrentState => _currentState;
		public Action onStateChanged;

		private void Start()
		{
			foreach (var transition in transitions)
			{
				if (transition.initialState != null) _availableStates.Add(transition.initialState);
				if (transition.transitionState != null) _availableStates.Add(transition.transitionState);
			}

			_availableStates.Add(initialState);
			_availableStates.Add(disabledState);

			foreach (var state in _availableStates)
			{
				state.Deactivate();
			}

			ResetSemaphore();
		}

		private void Update()
		{
			if (_currentState.IsFinished)
			{
				ChangeCurrentStateIfFinished();
				return;
			}

			ChangeCurrentStateIfConditionsMet();
		}

		public void ResetSemaphore()
		{
			SwitchState(initialState);
		}

		public void DisableSemaphore()
		{
			SwitchState(disabledState);
		}

		private void ChangeCurrentStateIfFinished()
		{
			foreach (var transition in transitions)
			{
				if (transition.initialState != _currentState) continue;
				if (!transition.IsAllConditionsSatisfied()) continue;

				SwitchState(transition.transitionState);
				return;
			}

			SwitchState(initialState);
		}

		private void ChangeCurrentStateIfConditionsMet()
		{
			foreach (var transition in transitions)
			{
				if (transition.initialState) continue;
				if (transition.transitionState == _currentState) continue;
				if (!transition.IsAllConditionsSatisfied()) continue;

				SwitchState(transition.transitionState);
				break;
			}
		}

		private void SwitchState(SemaphoreState newState)
		{
			if (_currentState != null) _currentState.Deactivate();
			_currentState = newState;
			_currentState.Activate();

			onStateChanged?.Invoke();
		}
	}
}