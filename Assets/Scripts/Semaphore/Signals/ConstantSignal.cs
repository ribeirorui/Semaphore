using System.Collections.Generic;
using UnityEngine;

namespace Semaphore.Signals
{
	/// <summary>
	/// Simple signal, always on, while activated
	/// </summary>
	public class ConstantSignal : Signal
	{
		[SerializeField] private List<GameObject> targetObjects;
		
		public override void Activate()
		{
			foreach (var targetObject in targetObjects)
			{
				targetObject.SetActive(true);
			}
		}

		public override void Deactivate()
		{
			foreach (var targetObject in targetObjects)
			{
				targetObject.SetActive(false);
			}
		}
	}
}