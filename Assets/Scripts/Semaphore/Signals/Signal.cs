using UnityEngine;

namespace Semaphore.Signals
{
	public abstract class Signal : MonoBehaviour
	{
		public abstract void Activate();
		public abstract void Deactivate();
	}
}