using System;
using System.Collections.Generic;
using Semaphore.State;

namespace Semaphore.Transitions
{
	[Serializable]
	public class Transition
	{
		public SemaphoreState initialState;
		public SemaphoreState transitionState;
		public List<TransitionCondition> conditions = new List<TransitionCondition>();

		public bool IsAllConditionsSatisfied()
		{
			//NonAlloc, without LINQ
			var allConditionSatisfied = true;
			foreach (var condition in conditions)
			{
				if (!condition.IsSatisfied())
				{
					allConditionSatisfied = false;
				}
			}

			return allConditionSatisfied;
		}
	}
}