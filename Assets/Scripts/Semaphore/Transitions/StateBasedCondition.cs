using Semaphore.State;
using UnityEngine;

namespace Semaphore.Transitions
{
	/// <summary>
	/// Set IsSatisfied depending on targetState.IsFinished condition
	/// </summary>
	public class StateBasedCondition : TransitionCondition
	{
		[SerializeField] private SemaphoreState targetState;
		[SerializeField] private bool satisfiedWhenTargetFinished;
		
		public override bool IsSatisfied()
		{
			return targetState.IsFinished == satisfiedWhenTargetFinished;
		}
	}
}