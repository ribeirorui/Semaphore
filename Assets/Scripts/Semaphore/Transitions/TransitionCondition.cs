using UnityEngine;

namespace Semaphore.Transitions
{
	public abstract class TransitionCondition : MonoBehaviour
	{
		public abstract bool IsSatisfied();
	}
}