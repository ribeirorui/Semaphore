using UnityEngine;
using UnityEngine.UI;

namespace Semaphore
{
	public class SemaphoreTextController : MonoBehaviour
	{
		[SerializeField] private SemaphoreController semaphore;
		[SerializeField] private Text targetText;

		private void Start()
		{
			semaphore.onStateChanged += UpdateText;
			UpdateText();
		}

		private void UpdateText()
		{
			//just for testing - taking state's gameObject name
			var semaphoreName = semaphore.CurrentState != null ? semaphore.CurrentState.gameObject.name : null;
			targetText.text = $"Current State: {semaphoreName}";
		}
	}
}